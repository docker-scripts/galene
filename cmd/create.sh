cmd_create_help() {
    cat <<_EOF
    create
        Create the container '$CONTAINER'.

_EOF
}

rename_function cmd_create orig_cmd_create
cmd_create() {
    local turn_port=${TURN_PORT:-1194}
    local udp_ports=${UDP_PORTS:-49160-49200}

    mkdir -p groups data recordings
    orig_cmd_create \
        --publish $turn_port:$turn_port/tcp --publish $turn_port:$turn_port/udp \
        --publish $udp_ports:$udp_ports/udp \
        --mount type=bind,src=$(pwd)/groups,dst=/home/galene/groups \
        --mount type=bind,src=$(pwd)/data,dst=/home/galene/data \
        --mount type=bind,src=$(pwd)/recordings,dst=/home/galene/recordings \
        "$@"
}
