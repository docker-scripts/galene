# Galene in a container

https://galene.org/

## Installation

  - First install `ds` and `revproxy`:
      + https://gitlab.com/docker-scripts/ds#installation
      + https://gitlab.com/docker-scripts/revproxy#installation

  - Then get the scripts: `ds pull galene`

  - Create a directory for the container: `ds init galene @galene.example.org`

  - Fix the settings: `cd /var/ds/galene.example.org/ ; vim settings.sh`

  - Make the container: `ds make`

## Configuration

- Configurations of the groups (meeting rooms) are at `groups/`. Make
  a copy of `groups/meeting.json` and modify it. Use symbolic links to
  create a group with the same settings but a different name. For more
  details about the configuration settings see:
  https://github.com/jech/galene#readme

- To improve the performance and reliability of Galene, you can add
  configurations for TURN servers, as described at the end of
  https://github.com/jech/galene/blob/master/INSTALL

## Usage

You can type `/help` on the chat to see some commads. If you logged in
as an **operator** you will have more commands available.

Some of the things that you can do as an operator are:

- Mute users with `/mute` or `/mute all`.

- Add or remove the ability to present to other users with `/present`
  and `/unpresent`. The ability to present means that users are able
  to enable their microphone and camera, and to share the screen. By
  the way, if you have the setting `"presenter": [{}],` in the group
  configuration, all the users will be able to join the group without
  a password and will be able to present.

- Lock and unlock a group with `/lock` and `/unlock`. Locking a group
  means that no more people will be able to join the group, besides
  those that are already present.

- Kick out a user with `/kick`.

- Start recording with `/record` and stop it with `/unrecord`. For
  this feature you need to have the setting `"allow-recording": true,`
  on the configuration of the group.

Any user can create subgroups, if you have the setting
`"allow-subgroups": true,` on the configuration of the group.

See also: https://galene.org/faq.html


## Other commands

```
ds stop
ds start
ds shell
```
