include(bookworm)

RUN apt install --yes git curl wget

### install go
RUN <<EOF
  wget https://go.dev/dl/go1.23.1.linux-amd64.tar.gz -O go.tar.gz
  tar -C /usr/local -xzf go.tar.gz
  rm -f go.tar.gz
EOF
ENV PATH $PATH:/usr/local/go/bin

### install galene
RUN <<EOF
  git clone https://github.com/jech/galene /root/galene
  cd /root/galene/
  CGO_ENABLED=0 go build -ldflags='-s -w'

  useradd -rm galene
  cp /root/galene/galene /home/galene/
  cp -a /root/galene/static/ /home/galene/
  chown galene: -R /home/galene
  rm -rf /root/galene/
EOF