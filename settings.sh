APP=galene

DOMAIN='galene.example.org'

### Uncomment to change the default values.
#TURN_PORT=1194
#UDP_PORTS=49160-49200

### Operator credentials.
OP_USER="admin"
OP_PASS="pass123"
