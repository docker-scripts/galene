#!/bin/bash -x

source /host/settings.sh

main() {
    create_config
    create_groups
    create_systemd_service
}

create_config() {
    local config_file=/home/galene/data/config.json
    [[ -f $config_file ]] || cat > $config_file <<EOF
{
    "admin":[{"username":"$OP_USER","password":"$OP_PASS"}],
    "canonicalHost": "$DOMAIN",
    "proxyURL": "https://$DOMAIN/"
}
EOF

    chown galene: -R /home/galene/data/
}

create_groups() {
    local group=/home/galene/groups/meeting.json
    [[ -f $group ]] || cat > $group <<EOF
{
    "op": [{"username": "$OP_USER", "password": "$OP_PASS"}],
    "presenter": [{}],
    "other": [{}],
    "public": true,
    "displayName": "Meeting",
    "description": "This is the default meeting group. For more details on group definitions see https://github.com/jech/galene#readme. For help on commands, type '/help' on the chat.",
    "autolock": false,
    "autokick": false,
    "allow-recording": true,
    "allow-subgroups": true,
    "allow-anonymous": false
}
EOF

    chown galene: -R /home/galene/groups/
    chown galene: -R /home/galene/recordings/
}

_get_public_ip() {
    local services='ifconfig.co ifconfig.me icanhazip.com'
    local pattern='^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$'
    local ip
    for service in $services; do
        ip=$(curl -s $service)
        [[ $ip =~ $pattern ]] && echo $ip && return
    done
    return 1
}

create_systemd_service() {
    local public_ip=$(_get_public_ip)
    local turn_port=${TURN_PORT:-1194}
    local udp_ports=${UDP_PORTS:-49160-49200}
    
    # create a systemd service for starting galene
    cat > /etc/systemd/system/galene.service <<EOF
[Unit]
Description=Galene
After=network.target

[Service]
Type=simple
WorkingDirectory=/home/galene
User=galene
Group=galene
ExecStart=/home/galene/galene -turn $public_ip:$turn_port -udp-range $udp_ports
LimitNOFILE=65536

[Install]
WantedBy=multi-user.target
EOF

    # enable and start the service
    systemctl daemon-reload
    systemctl enable galene.service
    systemctl start galene.service
}

# call main
main "$@"
